package mekamello.galleryviewer.ui.view;

public interface BaseView {
    void onAttachView();
    void onDetachView();
}
