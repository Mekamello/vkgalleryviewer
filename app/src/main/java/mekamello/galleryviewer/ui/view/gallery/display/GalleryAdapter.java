package mekamello.galleryviewer.ui.view.gallery.display;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import mekamello.galleryviewer.R;
import mekamello.galleryviewer.base.MainThreadImpl;
import mekamello.galleryviewer.base.ThreadExecutorImpl;
import mekamello.galleryviewer.data.database.models.Photo;
import mekamello.galleryviewer.data.repositories.PhotoMemoryRepositoryImpl;
import mekamello.galleryviewer.ui.presenter.gallery.display.GalleryPresenter;
import mekamello.galleryviewer.ui.presenter.gallery.element.GalleryViewHolderPresenter;
import mekamello.galleryviewer.ui.presenter.gallery.element.GalleryViewHolderPresenterImpl;
import mekamello.galleryviewer.ui.view.gallery.element.GalleryViewHolder;
import mekamello.galleryviewer.utils.DateUtils;


public class GalleryAdapter extends RecyclerView.Adapter<GalleryViewHolder> implements
        GalleryPresenter.View,
        View.OnClickListener{

    private GalleryPresenter presenter;

    private SortedList<Photo> data;

    private final Context context;

    public GalleryAdapter(@NonNull GalleryPresenter presenter, @NonNull Context context) {
        this.context = context;
        this.presenter = presenter;

        data = new SortedList<>(Photo.class, new SortedList.Callback<Photo>() {
            @Override
            public int compare(Photo o1, Photo o2) {
                return DateUtils.compare(o1, o2);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(Photo oldItem, Photo newItem) {
                return (oldItem != null && newItem != null) && oldItem.isContainEqualData(newItem);
            }

            @Override
            public boolean areItemsTheSame(Photo item1, Photo item2) {
                return item1.id == item2.id;
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }
        });

    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.view_gallery_image, parent, false);

        view.setOnClickListener(this);
        GalleryViewHolder viewHolder = new GalleryViewHolder(view);
        view.setTag(viewHolder);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GalleryViewHolder holder, int position) {
        GalleryViewHolderPresenter viewHolderPresenter = new GalleryViewHolderPresenterImpl(
                ThreadExecutorImpl.getInstance(),
                MainThreadImpl.getInstance(),
                PhotoMemoryRepositoryImpl.getInstance(context)
        );

        holder.inject(viewHolderPresenter);

        holder.bind(data.get(position));
    }


    @Override
    public void onViewRecycled(GalleryViewHolder holder) {
        super.onViewRecycled(holder);

        holder.onViewRecycled();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void putPhotos(@NonNull List<Photo> photos) {
        data.beginBatchedUpdates();
        data.addAll(photos);
        data.endBatchedUpdates();
    }

    @Override
    public void refreshPhotos() {
       notifyItemRangeChanged(0, data.size());
    }

    @Override
    public void clearPhotos() {
        data.clear();
    }

    @Override
    public void onClick(View v) {
        Object tag = v.getTag();
        if (tag instanceof GalleryViewHolder) {
            int position = ((GalleryViewHolder) tag).getAdapterPosition();
            presenter.selectPhoto(position);
        }
    }
}
