package mekamello.galleryviewer.ui.view.photo.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mekamello.galleryviewer.R;
import mekamello.galleryviewer.base.MainThreadImpl;
import mekamello.galleryviewer.base.ThreadExecutorImpl;
import mekamello.galleryviewer.data.database.models.Photo;
import mekamello.galleryviewer.data.repositories.PhotoMemoryRepositoryImpl;
import mekamello.galleryviewer.ui.presenter.photo.adapter.GalleryPreviewAdapterPresenter;
import mekamello.galleryviewer.ui.presenter.photo.display.GalleryPreviewPresenter;
import mekamello.galleryviewer.ui.presenter.photo.element.GalleryPreviewElementPresenter;
import mekamello.galleryviewer.ui.presenter.photo.element.GalleryPreviewElementPresenterImpl;
import mekamello.galleryviewer.ui.view.photo.element.GalleryPreviewElementViewImpl;
import mekamello.galleryviewer.utils.DateUtils;


public class GalleryPreviewAdapter extends PagerAdapter implements GalleryPreviewAdapterPresenter.View{
    private List<Photo> data = new ArrayList<>();

    private final GalleryPreviewAdapterPresenter previewAdapterPresenter;

    private final ViewPager viewPager;

    private final Context context;

    private final GalleryPreviewPresenter.FreeSpaceClickListener listener;

    public GalleryPreviewAdapter(@NonNull GalleryPreviewAdapterPresenter previewAdapterPresenter,
                                 @NonNull ViewPager viewPager,
                                 @NonNull Context context,
                                 @NonNull GalleryPreviewPresenter.FreeSpaceClickListener listener) {

        this.previewAdapterPresenter = previewAdapterPresenter;
        this.viewPager = viewPager;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View rootView = LayoutInflater
                .from(container.getContext())
                .inflate(R.layout.view_preview_image, container, false);


        GalleryPreviewElementPresenter elementPresenter = new GalleryPreviewElementPresenterImpl(
                ThreadExecutorImpl.getInstance(),
                MainThreadImpl.getInstance(),
                PhotoMemoryRepositoryImpl.getInstance(context)
        );

        GalleryPreviewElementPresenter.View view = new GalleryPreviewElementViewImpl(rootView);

        elementPresenter.bindView(view, data.get(position));

        container.addView(rootView);

        rootView.setOnClickListener((element) -> listener.onFreeSpaceClicked());

        return rootView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(((View) object));
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void putContent(@NonNull Cursor cursor, int position) {
        if(!data.isEmpty()) {
            data.clear();
        }

        while (cursor.moveToNext()) {
            Photo photo = new Photo(cursor);
            if(!data.contains(photo)) {
                data.add(photo);
            }
        }

        cursor.close();

        Collections.sort(data, DateUtils::compare);

        notifyDataSetChanged();

        viewPager.setCurrentItem(position);
    }

    @Override
    public void onErrorMessage(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

}
