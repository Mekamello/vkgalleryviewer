package mekamello.galleryviewer.ui.view.photo.adapter;

import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import mekamello.galleryviewer.R;
import mekamello.galleryviewer.ui.presenter.photo.adapter.GalleryPreviewAdapterPresenter;
import mekamello.galleryviewer.ui.presenter.photo.display.GalleryPreviewPresenter;

public class GalleryPreviewAdapterViewImpl implements GalleryPreviewPresenter.View {
    private final View view;

    private final GalleryPreviewAdapterPresenter adapterPresenter;

    private ViewPager slider;

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            adapterPresenter.setSelectedPosition(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    public GalleryPreviewAdapterViewImpl(@NonNull View view,
                                         @NonNull GalleryPreviewAdapterPresenter adapterPresenter,
                                         int position,
                                         @NonNull GalleryPreviewPresenter.FreeSpaceClickListener listener) {
        this.view = view;
        this.adapterPresenter = adapterPresenter;

        slider = (ViewPager) view.findViewById(R.id.slider);

        GalleryPreviewAdapter previewAdapter = new GalleryPreviewAdapter(
                adapterPresenter,
                slider,
                view.getContext(),
                listener);


        slider.setAdapter(previewAdapter);

        adapterPresenter.bindView(previewAdapter);

        adapterPresenter.setSelectedPosition(position);
    }

    @Override
    public void onAttachView() {
        adapterPresenter.onStart();

        slider.addOnPageChangeListener(pageChangeListener);
    }

    @Override
    public void onDetachView() {
        adapterPresenter.onStop();

        slider.removeOnPageChangeListener(pageChangeListener);
    }

    @Override
    public void onErrorMessage(String message) {
        Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
