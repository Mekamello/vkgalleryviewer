package mekamello.galleryviewer.ui.view.gallery.display;

import android.app.ActivityOptions;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import mekamello.galleryviewer.GalleryApplication;
import mekamello.galleryviewer.R;
import mekamello.galleryviewer.dagger.gallery.GalleryModule;
import mekamello.galleryviewer.data.database.models.Photo;
import mekamello.galleryviewer.ui.presenter.gallery.display.GalleryPresenter;
import mekamello.galleryviewer.ui.view.gallery.display.misc.EndlessScrollListener;
import mekamello.galleryviewer.ui.view.photo.display.GalleryPreviewActivity;


public class GalleryFragment extends Fragment implements GalleryView, SwipeRefreshLayout.OnRefreshListener {
    private static final String LOGGEDIN_ARG = "LOGGEDIN_ARG";

    @Inject
    GalleryPresenter galleryPresenter;

    private boolean isLoggedIn;

    private GalleryAdapter adapter;

    private TextView placeholder;

    private RecyclerView list;

    private SwipeRefreshLayout refreshLayout;

    private GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);

    private EndlessScrollListener scrollListener = new EndlessScrollListener(layoutManager) {
        @Override
        public void onLoadMore() {
            if(isNetworkActive()) {
                galleryPresenter.loadNextPhotos(0);
            }
        }
    };

    public static GalleryFragment newInstance(boolean isLoggedIn) {
        GalleryFragment fragment = new GalleryFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(LOGGEDIN_ARG, isLoggedIn);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        GalleryApplication
                .get(getActivity())
                .applicationComponent()
                .plus(new GalleryModule())
                .inject(this);


        adapter = new GalleryAdapter(galleryPresenter, getActivity());

        galleryPresenter.setInternetConnectionState(isNetworkActive());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);
        handleArguments(getArguments());

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);
        appCompatActivity.setTitle(R.string.gallery);

        refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(this);

        placeholder = (TextView) rootView.findViewById(R.id.placeholder);

        list = (RecyclerView) rootView.findViewById(R.id.list);
        list.setLayoutManager(layoutManager);
        list.setHasFixedSize(true);
        list.setAdapter(adapter);
        list.addOnScrollListener(scrollListener);

        galleryPresenter.bindView(this);
        galleryPresenter.setLoggedIn(isLoggedIn);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        list.setLayoutManager(null);
        list.removeOnScrollListener(scrollListener);
    }

    @Override
    public void onStart() {
        super.onStart();

        galleryPresenter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

        galleryPresenter.onStop();
    }

    @Override
    public void startPhotoPreview(int position) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
            GalleryPreviewActivity.start(getActivity(), position);
        } else {
            Bundle bundle = ActivityOptions.makeCustomAnimation(getActivity(), R.anim.fade_in, R.anim.fade_out).toBundle();
            GalleryPreviewActivity.start(getActivity(), bundle, position);
        }
    }

    @Override
    public void showPlaceholder() {
        placeholder.setVisibility(View.VISIBLE);
        list.setVisibility(View.GONE);
    }

    @Override
    public void showList() {
        placeholder.setVisibility(View.GONE);
        list.setVisibility(View.VISIBLE);
    }

    @Override
    public void onShowLoading() {
        refreshLayout.setRefreshing(true);
    }

    @Override
    public void onHideLoading() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onErrorMessage(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void putPhotos(@NonNull List<Photo> data) {
        adapter.putPhotos(data);
    }

    @Override
    public void refreshPhotos() {
        adapter.refreshPhotos();
    }

    @Override
    public void clearPhotos() {
        adapter.clearPhotos();
    }

    @Override
    public void checkInternetConnection() {
        Boolean isOnline = isNetworkActive();

        if(!isOnline) {
            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
        }

        galleryPresenter.setInternetConnectionState(isOnline);
    }

    @Override
    public void onRefresh() {
        galleryPresenter.refreshPhotos();
    }

    private void handleArguments(@Nullable Bundle args) {
        if(args != null) {
            isLoggedIn = args.getBoolean(LOGGEDIN_ARG);
        }
    }

    public boolean isNetworkActive() {
        ConnectivityManager cm = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}
