package mekamello.galleryviewer.ui.view.photo.element;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import mekamello.galleryviewer.R;
import mekamello.galleryviewer.ui.presenter.photo.element.GalleryPreviewElementPresenter;

public class GalleryPreviewElementViewImpl implements GalleryPreviewElementPresenter.View {

    private final View view;

    private final ImageView image;

    private final TextView description;

    private final TextView date;

    private final TextView title;

    private final ProgressBar progressBar;

    private final ViewGroup imageContainer;

    public GalleryPreviewElementViewImpl(@NonNull View view) {
        this.view = view;

        image = (ImageView) view.findViewById(R.id.image);

        description = (TextView) view.findViewById(R.id.description);

        title = (TextView) view.findViewById(R.id.title);

        progressBar = (ProgressBar) view.findViewById(R.id.progress);

        date = (TextView) view.findViewById(R.id.date);

        imageContainer = (ViewGroup) view.findViewById(R.id.image_container);
    }

    @Override
    public void setImage(@NonNull Bitmap bitmap) {
        image.setImageBitmap(bitmap);
    }

    @Override
    public void setDescription(@NonNull String text) {
        description.setText(text);
    }

    @Override
    public void setDate(@NonNull String dateToString) {
        date.setText(dateToString);
    }

    @Override
    public void onShowLoading() {
        image.setImageResource(R.drawable.placeholder);
        imageContainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading() {
        imageContainer.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onShowDescription() {
        title.setVisibility(View.VISIBLE);
        description.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideDescription() {
        title.setVisibility(View.GONE);
        description.setVisibility(View.GONE);
    }

}
