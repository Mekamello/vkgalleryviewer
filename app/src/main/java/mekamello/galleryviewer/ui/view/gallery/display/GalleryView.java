package mekamello.galleryviewer.ui.view.gallery.display;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.api.VKError;

import mekamello.galleryviewer.ui.presenter.gallery.display.GalleryPresenter;

public interface GalleryView extends GalleryPresenter.View{

    void startPhotoPreview(int position);

    void checkInternetConnection();

    void showPlaceholder();
    void showList();

    void onShowLoading();
    void onHideLoading();

    void onErrorMessage(String error);

}
