package mekamello.galleryviewer.ui.view.photo.display;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import javax.inject.Inject;

import mekamello.galleryviewer.GalleryApplication;
import mekamello.galleryviewer.R;
import mekamello.galleryviewer.dagger.gallery.GalleryPreviewModule;
import mekamello.galleryviewer.ui.presenter.photo.adapter.GalleryPreviewAdapterPresenter;
import mekamello.galleryviewer.ui.presenter.photo.display.GalleryPreviewPresenter;
import mekamello.galleryviewer.ui.view.photo.adapter.GalleryPreviewAdapterViewImpl;


public class GalleryPreviewActivity extends AppCompatActivity {
    public static final String EXTRA_PREVIEW_POSITION = "mekamello.galleryviewer.ui.router.PreviewPositionExtra";

    public static final String PREVIEW_POSITION_SAVE = "mekamello.galleryviewer.ui.router.PreviewPositionSave";

    @Inject
    GalleryPreviewPresenter previewPresenter;

    @Inject
    GalleryPreviewAdapterPresenter adapterPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }

        setTitle("");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setFinishOnTouchOutside(true);

        GalleryApplication
                .get(getApplicationContext())
                .applicationComponent()
                .plus(new GalleryPreviewModule())
                .inject(this);

        Intent intent = getIntent();

        int position = 0;

        if(intent != null) {
            position = intent.getIntExtra(EXTRA_PREVIEW_POSITION, position);
        } else if (savedInstanceState != null) {
            position = savedInstanceState.getInt(PREVIEW_POSITION_SAVE);

        }

        GalleryPreviewPresenter.View view = new GalleryPreviewAdapterViewImpl(
                getWindow().getDecorView().getRootView(),
                adapterPresenter,
                position,
                this::finish);

        previewPresenter.bindView(view);
    }


    @Override
    protected void onStart() {
        super.onStart();

        previewPresenter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

        previewPresenter.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(PREVIEW_POSITION_SAVE, previewPresenter.getCurrentViewedPhotoPosition());
    }

    public static Intent createIntent(@NonNull Context context, int position) {
        Intent intent = new Intent(context, GalleryPreviewActivity.class);
        intent.putExtra(EXTRA_PREVIEW_POSITION, position);
        return intent;
    }

    public static void start(@NonNull Context context, int position) {
        context.startActivity(createIntent(context, position));
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public static void start(@NonNull Context context, @NonNull Bundle bundle, int position) {
        context.startActivity(createIntent(context, position), bundle);
    }
}
