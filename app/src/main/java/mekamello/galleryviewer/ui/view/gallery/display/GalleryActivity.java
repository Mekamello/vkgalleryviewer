package mekamello.galleryviewer.ui.view.gallery.display;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.model.VKScopes;

import mekamello.galleryviewer.R;

import static mekamello.galleryviewer.GalleryApplication.INVALID_TOKEN_EXTRA;

public class GalleryActivity extends AppCompatActivity {
    public static final String BROADCAST_INVALID_TOKEN = "mekamello.galleryviewer.ui.view.gallery.display.BROADCAST_INVALID_TOKEN";
    public static final String LOGOUT_DIALOG_STATE = "mekamello.galleryviewer.ui.view.gallery.display.LOGOUT_DIALOG_STATE";

    private MenuItem login;
    private MenuItem logout;

    private boolean isLogoutShowing;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent != null) {
                boolean invalidToken = intent.getBooleanExtra(INVALID_TOKEN_EXTRA, false);
                if (invalidToken) sendLoggedResult(false);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        if(savedInstanceState == null) {
            sendLoggedResult(VKSdk.isLoggedIn());
        }

        IntentFilter intentFilter = new IntentFilter(BROADCAST_INVALID_TOKEN);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(receiver);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(!VKSdk.onActivityResult(requestCode, resultCode, data, new  VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                sendLoggedResult(true);
            }

            @Override
            public void onError(VKError error) {
                sendLoggedResult(false);
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        isLogoutShowing = savedInstanceState.getBoolean(LOGOUT_DIALOG_STATE);

        if(isLogoutShowing) {
            makeLogout();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(LOGOUT_DIALOG_STATE, isLogoutShowing);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        logout = menu.findItem(R.id.logout);
        login = menu.findItem(R.id.login);

        refreshMenuButtons();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.login:
                makeLogin();
                refreshMenuButtons();
                return true;
            case R.id.logout:
                makeLogout();
                refreshMenuButtons();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void makeLogin(){
        VKSdk.login(this, VKScopes.PHOTOS);
    }

    public void makeLogout() {
        isLogoutShowing = true;

        AlertDialog logoutDialog = new AlertDialog
                .Builder(this, R.style.LogoutDialog)
                .setMessage(R.string.question_logout)
                .setCancelable(true)
                .setNegativeButton(R.string.answer_no, (dialog, which) -> dialog.dismiss())
                .setPositiveButton(R.string.answer_yes, ((dialog, which) -> {
                    VKSdk.logout();

                    sendLoggedResult(false);

                    refreshMenuButtons();

                    dialog.dismiss();
                }))
                .setOnDismissListener((dialogInterface) -> isLogoutShowing = false)
                .create();

        logoutDialog.show();
    }

    private void sendLoggedResult(boolean isLoggedIn) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, GalleryFragment.newInstance(isLoggedIn))
                .commit();
    }

    private void refreshMenuButtons() {
        if(VKSdk.isLoggedIn()) {
            login.setVisible(false);
            logout.setVisible(true);
        } else {
            login.setVisible(true);
            logout.setVisible(false);
        }

    }
}
