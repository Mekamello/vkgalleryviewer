package mekamello.galleryviewer.ui.view.gallery.element;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import mekamello.galleryviewer.R;
import mekamello.galleryviewer.data.database.models.Photo;
import mekamello.galleryviewer.ui.presenter.gallery.element.GalleryViewHolderPresenter;


public class GalleryViewHolder extends RecyclerView.ViewHolder implements GalleryViewHolderPresenter.View{
    private final ImageView imageView;

    private final ProgressBar progressBar;

    public GalleryViewHolderPresenter presenter;

    public GalleryViewHolder(View itemView) {
        super(itemView);

        imageView = (ImageView) itemView.findViewById(R.id.image);

        progressBar = (ProgressBar) itemView.findViewById(R.id.progress);
    }

    public void inject(@NonNull GalleryViewHolderPresenter presenter) {
        this.presenter = presenter;
    }

    public void bind(@NonNull Photo photo) {
        presenter.bindView(this, photo);
    }

    @Override
    public void onShow(@NonNull Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public void onError(@NonNull String message) {
        Log.e("GalleryViewHolder", "Some issues happen");
    }

    @Override
    public void onShowLoading() {
        imageView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading() {
        imageView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    public void onViewRecycled() {
        presenter.cancelLoading();
    }

    @Override
    public int getWidth() {
        return 150;
    }

    @Override
    public int getHeight() {
        return 150;
    }
}
