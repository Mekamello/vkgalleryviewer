package mekamello.galleryviewer.ui.presenter.photo.display;

import android.support.annotation.NonNull;

import mekamello.galleryviewer.ui.presenter.photo.adapter.GalleryPreviewAdapterPresenter;

public class GalleryPreviewPresenterImpl implements GalleryPreviewPresenter {

    private View view;

    private final GalleryPreviewAdapterPresenter adapterPresenter;

    public GalleryPreviewPresenterImpl(@NonNull GalleryPreviewAdapterPresenter adapterPresenter) {
        this.adapterPresenter = adapterPresenter;
    }

    @Override
    public void onStart() {
        view.onAttachView();
    }

    @Override
    public void onStop() {
        view.onDetachView();
    }

    @Override
    public void bindView(@NonNull View view) {
        this.view = view;
    }

    @Override
    public int getCurrentViewedPhotoPosition() {
        return adapterPresenter.getSelectedPosition();
    }
}
