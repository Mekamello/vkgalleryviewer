package mekamello.galleryviewer.ui.presenter.photo.adapter;

import android.database.Cursor;
import android.support.annotation.NonNull;

import java.util.List;

import mekamello.galleryviewer.data.database.models.Photo;

public interface GalleryPreviewAdapterPresenter {

    void onStart();

    void onStop();

    void bindView(View view);

    void refresh();

    void closePhoto();

    void setSelectedPosition(int position);

    int getSelectedPosition();

    interface View {
        void putContent(@NonNull Cursor cursor, int position);

        void onErrorMessage(String message);

    }
}
