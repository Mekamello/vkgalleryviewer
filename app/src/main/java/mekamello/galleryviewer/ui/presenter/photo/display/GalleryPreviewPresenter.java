package mekamello.galleryviewer.ui.presenter.photo.display;

import android.support.annotation.NonNull;

public interface GalleryPreviewPresenter {

    void onStart();

    void onStop();

    void bindView(@NonNull View view);

    int getCurrentViewedPhotoPosition();

    interface View {
        void onAttachView();
        void onDetachView();
        void onErrorMessage(String message);
    }

    interface FreeSpaceClickListener {
       void onFreeSpaceClicked();
    }

}
