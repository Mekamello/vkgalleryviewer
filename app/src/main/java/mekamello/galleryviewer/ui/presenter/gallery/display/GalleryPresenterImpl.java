package mekamello.galleryviewer.ui.presenter.gallery.display;


import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mekamello.galleryviewer.base.Executor;
import mekamello.galleryviewer.base.MainThread;
import mekamello.galleryviewer.buisness.GalleryInteractor;
import mekamello.galleryviewer.buisness.GalleryInteractorImpl;
import mekamello.galleryviewer.data.database.models.Photo;
import mekamello.galleryviewer.data.repositories.GalleryRepository;
import mekamello.galleryviewer.ui.view.gallery.display.GalleryView;


public class GalleryPresenterImpl implements GalleryPresenter {
    private static final int COUNT_TO_LOAD = 5;

    @NonNull
    private final GalleryInteractor.Callback callback = new GalleryInteractor.Callback() {
        @Override
        public void onSuccess(List<Photo> data, int maxCount) {
            maxCountContains = maxCount;

            handlePhotos(data);

            view.onHideLoading();
        }

        @Override
        public void onFailure(Throwable error) {
            handleError(error);

            view.onHideLoading();
        }
    };

    private List<Photo> data = new ArrayList<>();

    @NonNull
    private final GalleryInteractor galleryInteractor;

    private GalleryView view;

    private boolean isLoggedIn;

    private boolean lastInternetConnectionStateOnline;

    private int offset = 0;
    private int maxCountContains;

    public GalleryPresenterImpl(@NonNull Executor executor,
                                @NonNull MainThread mainThread,
                                @NonNull GalleryRepository repository) {

        galleryInteractor = new GalleryInteractorImpl(executor, mainThread, callback, repository);
    }

    @Override
    public void onStart() {
        if(data.isEmpty() && isLoggedIn && lastInternetConnectionStateOnline) {
            processLoading(offset);
        }

        checkConnection();

        refreshViewContent();
    }

    @Override
    public void onStop() {

    }

    @Override
    public void bindView(GalleryView view) {
        this.view = view;
    }

    @Override
    public void setLoggedIn(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    @Override
    public void setInternetConnectionState(boolean isOnline) {
        lastInternetConnectionStateOnline = isOnline;
    }

    @Override
    public void loadNextPhotos(int currentPosition) {
        if(data.size() < maxCountContains) {
            offset = offset + COUNT_TO_LOAD;
            galleryInteractor.load(offset, COUNT_TO_LOAD);
        }

        if(data.size() == maxCountContains) {
            offset = 0;
        }

        if(data.size() > maxCountContains) {
            offset = 0;
            galleryInteractor.load(offset, data.size());
        }
    }

    @Override
    public void refreshPhotos() {
        checkConnection();

        if(lastInternetConnectionStateOnline) {
            offset = 0;
            processLoading(offset);
        } else {
            view.onHideLoading();
        }
    }

    @Override
    public void clearPhotos() {
        view.clearPhotos();
    }

    @Override
    public void selectPhoto(int position) {
        view.startPhotoPreview(position);
    }

    private void processLoading(int offset) {
        view.onShowLoading();

        galleryInteractor.load(offset, COUNT_TO_LOAD);
    }

    private void handlePhotos(List<Photo> photos) {
        if(!photos.isEmpty()) {

            for(Photo photo : photos) {
                if(!data.contains(photo)) {
                    data.add(photo);
                }
            }

            view.putPhotos(data);
        }

        checkConnection();

        refreshViewContent();
    }

    private void handleError(Throwable message) {
//        Log.e("vkGalleryViewer", "GalleryInteractor", message);

        view.onErrorMessage(message.getMessage());

        checkConnection();

        refreshViewContent();
    }

    private void checkConnection() {
        if(!lastInternetConnectionStateOnline) {
            view.refreshPhotos();
        }

        view.checkInternetConnection();
    }

    private void refreshViewContent() {
        if(data.isEmpty()) {
            view.showPlaceholder();
        } else {
            view.showList();
        }
    }
}
