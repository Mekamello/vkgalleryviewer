package mekamello.galleryviewer.ui.presenter.gallery.display;

import android.support.annotation.NonNull;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.api.VKError;

import java.util.List;

import mekamello.galleryviewer.data.database.models.Photo;
import mekamello.galleryviewer.state.GalleryPresenterState;
import mekamello.galleryviewer.ui.view.gallery.display.GalleryView;


public interface GalleryPresenter {
    void onStart();
    void onStop();

    void bindView(GalleryView view);

    void setLoggedIn(boolean isLoggedIn);

    void setInternetConnectionState(boolean isOnline);

    void loadNextPhotos(int offset);

    void refreshPhotos();

    void clearPhotos();

    void selectPhoto(int position);

    interface View {
        void putPhotos(@NonNull List<Photo> data);

        void refreshPhotos();

        void clearPhotos();
    }
}
