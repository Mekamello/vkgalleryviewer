package mekamello.galleryviewer.ui.presenter.photo.element;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import mekamello.galleryviewer.base.Executor;
import mekamello.galleryviewer.base.MainThread;
import mekamello.galleryviewer.buisness.PhotoInteractor;
import mekamello.galleryviewer.buisness.PhotoInteractorImpl;
import mekamello.galleryviewer.data.database.models.Photo;
import mekamello.galleryviewer.data.repositories.PhotoMemoryRepository;
import mekamello.galleryviewer.utils.DateUtils;

public class GalleryPreviewElementPresenterImpl implements GalleryPreviewElementPresenter {
    private final PhotoInteractor.Callback callback = new PhotoInteractor.Callback() {
        @Override
        public void onSuccess(Bitmap bitmap, Photo photo) {
            view.setImage(bitmap);

            view.onHideLoading();

            if(photo.text != null && !TextUtils.isEmpty(photo.text)) {
                view.onShowDescription();

                view.setDescription(photo.text);
            } else {
                view.onHideDescription();
            }

            view.setDate(DateUtils.getDateFromSeconds((long)photo.date));

        }

        @Override
        public void onFailure(String message) {

        }
    };

    private final PhotoInteractor photoInteractor;

    private View view;

    private Photo currentPhoto;

    private final Photo.Size size = Photo.Size.Photo1280;

    public GalleryPreviewElementPresenterImpl(@NonNull Executor executor,
                                              @NonNull MainThread mainThread,
                                              @NonNull PhotoMemoryRepository repository) {

        photoInteractor = new PhotoInteractorImpl(executor, mainThread, callback, repository);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void bindView(@NonNull View view, @NonNull Photo photo) {
        this.view = view;
        this.currentPhoto = photo;

        view.onShowLoading();

        photoInteractor.getBitmap(currentPhoto, size);

    }
}
