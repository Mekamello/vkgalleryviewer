package mekamello.galleryviewer.ui.presenter.photo.adapter;

import android.database.Cursor;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mekamello.galleryviewer.base.Executor;
import mekamello.galleryviewer.base.MainThread;
import mekamello.galleryviewer.buisness.PhotoPreviewInteractor;
import mekamello.galleryviewer.buisness.PhotoPreviewInteractorImpl;
import mekamello.galleryviewer.data.database.models.Photo;
import mekamello.galleryviewer.data.repositories.PhotoProviderRepositoryImpl;

public class GalleryPreviewAdapterPresenterImpl implements GalleryPreviewAdapterPresenter {
    private final PhotoPreviewInteractor.Callback callback = new PhotoPreviewInteractor.Callback() {
        @Override
        public void onSuccess(Cursor cursor) {
            handleCursor(cursor);
        }

        @Override
        public void onFailure(String message) {

        }
    };

    private final PhotoPreviewInteractor previewInteractor;

    private View view;

    private int position;

    public GalleryPreviewAdapterPresenterImpl(@NonNull Executor executor,
                                              @NonNull MainThread mainThread,
                                              @NonNull PhotoProviderRepositoryImpl repository) {

        previewInteractor = new PhotoPreviewInteractorImpl(executor, mainThread, callback, repository);
    }


    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void bindView(View view) {
        this.view = view;

        refresh();
    }

    @Override
    public void refresh() {
       loadPhotos();
    }

    @Override
    public void closePhoto() {

    }

    @Override
    public void setSelectedPosition(int position) {
        this.position = position;
    }

    @Override
    public int getSelectedPosition() {
        return position;
    }


    private void loadPhotos() {
        previewInteractor.getCursor();
    }

    private void handleCursor(@NonNull Cursor cursor) {
        view.putContent(cursor, position);
    }
}
