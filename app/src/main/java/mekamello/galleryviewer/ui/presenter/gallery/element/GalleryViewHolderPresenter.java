package mekamello.galleryviewer.ui.presenter.gallery.element;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import mekamello.galleryviewer.data.database.models.Photo;


public interface GalleryViewHolderPresenter {
    void bindView(@NonNull View view, @NonNull Photo photo);

    void cancelLoading();

    interface View {
        void onShow(@NonNull Bitmap bitmap);
        void onError(@NonNull String message);
        void onShowLoading();
        void onHideLoading();
        int getWidth();
        int getHeight();
    }
}
