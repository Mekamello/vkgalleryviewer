package mekamello.galleryviewer.ui.presenter.photo.element;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import mekamello.galleryviewer.data.database.models.Photo;

public interface GalleryPreviewElementPresenter {
    void onStart();

    void onStop();

    void bindView(@NonNull View view, @NonNull Photo photo);

    interface View {

        void setImage(@NonNull Bitmap bitmap);

        void setDescription(@NonNull String text);

        void setDate(@NonNull String dateToString);

        void onShowLoading();

        void onHideLoading();

        void onShowDescription();

        void onHideDescription();

    }
}
