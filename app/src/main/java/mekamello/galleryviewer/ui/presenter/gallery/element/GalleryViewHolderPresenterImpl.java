package mekamello.galleryviewer.ui.presenter.gallery.element;


import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import mekamello.galleryviewer.buisness.PhotoInteractor;
import mekamello.galleryviewer.buisness.PhotoInteractorImpl;
import mekamello.galleryviewer.data.database.models.Photo;
import mekamello.galleryviewer.data.repositories.PhotoMemoryRepository;
import mekamello.galleryviewer.base.Executor;
import mekamello.galleryviewer.base.MainThread;

public class GalleryViewHolderPresenterImpl implements GalleryViewHolderPresenter {
    private final PhotoInteractor.Callback callback = new PhotoInteractor.Callback() {
        @Override
        public void onSuccess(Bitmap bitmap, Photo photo) {
            view.onShow(bitmap);
            view.onHideLoading();
        }

        @Override
        public void onFailure(String message) {
            view.onError(message);
        }
    };

    @Nullable
    private Photo currentPhoto;

    private final Photo.Size size = Photo.Size.Photo807;

    @NonNull
    private final PhotoInteractor photoInteractor;

    private GalleryViewHolderPresenter.View view;

    public GalleryViewHolderPresenterImpl(@NonNull Executor executor,
                                          @NonNull MainThread mainThread,
                                          @NonNull PhotoMemoryRepository repository) {

        photoInteractor = new PhotoInteractorImpl(executor, mainThread, callback, repository);
    }

    @Override
    public void bindView(@NonNull View view, @NonNull Photo newPhoto) {
        this.view = view;

        currentPhoto = newPhoto;

        startLoading(currentPhoto);
    }

    @Override
    public void cancelLoading() {
        photoInteractor.cancelLoading();
    }

    private void startLoading(@NonNull Photo photo) {
        view.onShowLoading();

        photoInteractor.getBitmap(photo, size, view.getWidth(), view.getHeight());
    }
}
