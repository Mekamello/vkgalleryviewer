package mekamello.galleryviewer.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import mekamello.galleryviewer.data.database.models.Photo;


public class DateUtils {
    private static final DateFormat DATE_FORMATTER = SimpleDateFormat.getDateInstance(DateFormat.SHORT);

    public static String getDateFromSeconds(long seconds){
        long millis = TimeUnit.SECONDS.toMillis(seconds);
        return DATE_FORMATTER.format(new Date(millis));
    }

    public static String getDateFromMilliseconds(long milliseconds){
        return DATE_FORMATTER.format(new Date(milliseconds));
    }

    public static String getDate(Date date){
        return DATE_FORMATTER.format(date);
    }

    public static int compare(Photo photo1, Photo photo2) {
        if (photo1 == null && photo2 == null) {
            return 0;
        }

        if (photo1 == null) {
            return -1;
        }

        if (photo2 == null) {
            return 1;
        }

        return (photo1.date > photo2.date ? -1 : (photo1.date == photo2.date ? 0 : 1));
    }
}
