package mekamello.galleryviewer.buisness;

import java.util.List;

import mekamello.galleryviewer.data.database.models.Photo;

public interface GalleryInteractor {

    void load(int offset, int count);

    interface Callback {
        void onSuccess(List<Photo> data, int maxCount);
        void onFailure(Throwable error);
    }
}
