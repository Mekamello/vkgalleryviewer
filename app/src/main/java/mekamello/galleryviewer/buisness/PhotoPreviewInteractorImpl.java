package mekamello.galleryviewer.buisness;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import mekamello.galleryviewer.base.BaseInteractor;
import mekamello.galleryviewer.base.Executor;
import mekamello.galleryviewer.base.MainThread;
import mekamello.galleryviewer.data.repositories.PhotoProviderRepositoryImpl;

public class PhotoPreviewInteractorImpl extends BaseInteractor implements PhotoPreviewInteractor {
    private final Callback callback;

    private final PhotoProviderRepositoryImpl repository;

    public PhotoPreviewInteractorImpl(@NonNull Executor executor,
                                      @NonNull MainThread mainThread,
                                      @NonNull Callback callback,
                                      @NonNull PhotoProviderRepositoryImpl repository) {
        super(executor, mainThread);

        this.callback = callback;
        this.repository = repository;
    }

    @Override
    public void getCursor() {
        execute();
    }

    @Override
    public void run() {
        mainThread.post(() -> repository.refresh(this::retrieve));
    }

    private void retrieve(@Nullable Cursor cursor) {
        if(cursor == null) {
            callback.onFailure("Didn't find cursor");
        } else {
            callback.onSuccess(cursor);
        }
    }
}
