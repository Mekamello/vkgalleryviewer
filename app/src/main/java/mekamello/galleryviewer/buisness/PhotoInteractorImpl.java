package mekamello.galleryviewer.buisness;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import mekamello.galleryviewer.base.BaseInteractor;
import mekamello.galleryviewer.base.Executor;
import mekamello.galleryviewer.base.MainThread;
import mekamello.galleryviewer.data.database.models.Photo;
import mekamello.galleryviewer.data.repositories.PhotoMemoryRepository;

public class PhotoInteractorImpl extends BaseInteractor implements PhotoInteractor {
    private final Callback callback;
    private final PhotoMemoryRepository repository;

    private Photo photo;

    private Photo.Size size;

    @Nullable
    private Options options;

    public PhotoInteractorImpl(@NonNull Executor executor,
                               @NonNull MainThread mainThread,
                               @NonNull Callback callback,
                               @NonNull PhotoMemoryRepository repository) {
        super(executor, mainThread);

        this.callback = callback;
        this.repository = repository;
    }

    @Override
    public void run() {
        if (isCanceled) return;

        Bitmap bitmap = options == null ?
                repository.getBitmap(photo, size) :
                repository.getBitmap(photo, size, options.width, options.height);

        if (isCanceled) return;

        mainThread.post(() -> {
            if (bitmap == null) {
                callback.onFailure("Can't loaded image");
            } else {
                callback.onSuccess(bitmap, photo);
            }

            options = null;
        });
    }

    @Override
    public void getBitmap(@NonNull Photo photo, @NonNull Photo.Size size) {
        this.photo = photo;
        this.size = size;

        execute();
    }

    @Override
    public void getBitmap(@NonNull Photo photo, @NonNull Photo.Size size, int width, int height) {
        this.options = new Options(width, height);
        this.photo = photo;
        this.size = size;

        execute();
    }

    @Override
    public void cancelLoading() {
        cancel();
    }

    private class Options {
        final int width;
        final int height;

        Options(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }
}
