package mekamello.galleryviewer.buisness;

import android.support.annotation.NonNull;

import mekamello.galleryviewer.data.repositories.GalleryRepository;
import mekamello.galleryviewer.base.BaseInteractor;
import mekamello.galleryviewer.base.Executor;
import mekamello.galleryviewer.base.MainThread;

public class GalleryInteractorImpl extends BaseInteractor implements GalleryInteractor{
    private final Callback callback;
    private final GalleryRepository repository;

    public GalleryInteractorImpl(@NonNull Executor executor,
                                 @NonNull MainThread mainThread,
                                 @NonNull Callback callback,
                                 @NonNull GalleryRepository repository) {
        super(executor, mainThread);

        this.repository = repository;
        this.callback = callback;
    }

    @Override
    public void run() {

    }

    @Override
    public void load(int offset, int count) {
        repository.getPhotos(offset, count, callback);
    }
}
