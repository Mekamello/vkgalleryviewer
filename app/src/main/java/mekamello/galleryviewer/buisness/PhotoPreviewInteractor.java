package mekamello.galleryviewer.buisness;

import android.database.Cursor;

public interface PhotoPreviewInteractor {

    void getCursor();

    interface Callback {
        void onSuccess(Cursor cursor);
        void onFailure(String message);
    }
}
