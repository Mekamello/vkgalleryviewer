package mekamello.galleryviewer.buisness;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import mekamello.galleryviewer.data.database.models.Photo;

public interface PhotoInteractor {

    void getBitmap(@NonNull Photo photo, @NonNull Photo.Size size);
    void getBitmap(@NonNull Photo photo, @NonNull Photo.Size size, int width, int height);

    void cancelLoading();

    interface Callback {
        void onSuccess(Bitmap bitmap, Photo photo);
        void onFailure(String message);
    }
}
