package mekamello.galleryviewer.data.network.models;


import com.google.gson.annotations.SerializedName;

import java.util.List;


public class PhotoResponse {
    @SerializedName("response")
    public Response get;

    public class Response {
        @SerializedName("count")
        public int count;
        @SerializedName("items")
        public List<PhotoModel> payload;
    }
}
