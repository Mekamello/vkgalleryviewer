package mekamello.galleryviewer.data.network;

import mekamello.galleryviewer.data.network.models.PhotoResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface PhotoVkService {

    @GET("photos.getAll")
    Call<PhotoResponse> getAllPhotos(
            @Query("access_token") String token,
            @Query("offset") String offset,
            @Query("count") String count,
            @Query("v") String apiVer);

}
