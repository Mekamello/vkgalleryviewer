package mekamello.galleryviewer.data.network.models;

import com.google.gson.annotations.SerializedName;

public class PhotoSize {
    @SerializedName("src")
    public String src;
    @SerializedName("width")
    public int width;
    @SerializedName("height")
    public int height;
    @SerializedName("type")
    public String type;
}
