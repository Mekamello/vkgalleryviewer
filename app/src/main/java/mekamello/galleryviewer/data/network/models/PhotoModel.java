package mekamello.galleryviewer.data.network.models;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PhotoModel {
    @SerializedName("id")
    public int id;
    @SerializedName("album_id")
    public int albumId;
    @SerializedName("owner_id")
    public int ownerId;
    @SerializedName("user_id")
    public int userId;
    @SerializedName("text")
    public String text;
    @SerializedName("date")
    public int timestamp;
    @SerializedName("sizes")
    public List<PhotoSize> sizes;
    @SerializedName("photo_75")
    public String photo75;
    @SerializedName("photo_130")
    public String photo130;
    @SerializedName("photo_604")
    public String photo604;
    @SerializedName("photo_807")
    public String photo807;
    @SerializedName("photo_1280")
    public String photo1280;
    @SerializedName("photo_2560")
    public String photo2560;
    @SerializedName("width*")
    public int width;
    @SerializedName("height*")
    public int height;
}
