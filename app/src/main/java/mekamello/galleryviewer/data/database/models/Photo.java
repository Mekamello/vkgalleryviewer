package mekamello.galleryviewer.data.database.models;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.Date;

import mekamello.galleryviewer.data.database.PhotoDatabase;


public class Photo {
    public enum Size { Photo75, Photo130, Photo604, Photo807, Photo1280, Photo2560}

    public final int id;
    public final int albumId; // convert to negative value if contains positive
    public final int ownerId;
    public final String text;
    public final int date;
    public final String photo75;
    public final String photo130;
    public final String photo604;
    public final String photo807;
    public final String photo1280;
    public final String photo2560;
    int width;
    int height;

    public Photo (@NonNull Cursor cursor) {
        id = cursor.getInt(cursor.getColumnIndex(PhotoDatabase.PHOTO_ORIGINAL_ID));
        albumId = cursor.getInt(cursor.getColumnIndex(PhotoDatabase.PHOTO_ALBUM_ID));
        ownerId = cursor.getInt(cursor.getColumnIndex(PhotoDatabase.PHOTO_OWNER_ID));
        text = cursor.getString(cursor.getColumnIndex(PhotoDatabase.PHOTO_TEXT));
        date =  cursor.getInt(cursor.getColumnIndex(PhotoDatabase.PHOTO_DATE));
        photo75 = cursor.getString(cursor.getColumnIndex(PhotoDatabase.PHOTO_SIZE_75));
        photo130 = cursor.getString(cursor.getColumnIndex(PhotoDatabase.PHOTO_SIZE_130));
        photo604 = cursor.getString(cursor.getColumnIndex(PhotoDatabase.PHOTO_SIZE_604));
        photo807 = cursor.getString(cursor.getColumnIndex(PhotoDatabase.PHOTO_SIZE_807));
        photo1280 = cursor.getString(cursor.getColumnIndex(PhotoDatabase.PHOTO_SIZE_1280));
        photo2560 = cursor.getString(cursor.getColumnIndex(PhotoDatabase.PHOTO_SIZE_2560));
    }

    private Photo(@NonNull Builder builder) {
        id = builder.id;
        albumId = builder.albumId;
        ownerId = builder.ownerId;
        text = builder.text;
        date = builder.date;
        photo75 = builder.photo75;
        photo130 = builder.photo130;
        photo604 = builder.photo604;
        photo807 = builder.photo807;
        photo1280 = builder.photo1280;
        photo2560 = builder.photo2560;
        width = builder.width;
        height = builder.height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public int hashCode() {
        int result = 1;

        result = 31 * result + (id ^ (id >>> 31));
        result = 31 * result + (albumId ^ (albumId >>> 31));
        result = 31 * result + (ownerId ^ (ownerId >>> 31));
        result = 31 * result + (date ^ (date >>> 31));

        result = 31 * result + (width ^ (width >>> 31));
        result = 31 * result + (height ^ (height >>> 31));

        result = 31 * result + text.hashCode();

        result = 31 * result + photo75.hashCode();
        result = 31 * result + photo130.hashCode();
        result = 31 * result + photo604.hashCode();
        result = 31 * result + photo807.hashCode();
        result = 31 * result + photo1280.hashCode();
        result = 31 * result + photo2560.hashCode();

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) return false;
        if (obj == this) return true;

        Photo another = ((Photo) obj);

        if(another.id != id) return  false;
        int anotherAlbumId = Math.abs(another.albumId);
        int currentAlbumId = Math.abs(albumId);

        if(anotherAlbumId != currentAlbumId) return  false;
        if(another.ownerId != ownerId) return  false;
        if(another.width != width) return false;
        if(another.height != height) return false;
        if(another.date != date) return false;


        return true;
    }

    public boolean isContainEqualData(Photo another) {
        if(!this.equals(another)) return false;

        if(!TextUtils.equals(another.text, text)) return false;
        if(!TextUtils.equals(another.photo75, photo75)) return false;
        if(!TextUtils.equals(another.photo130, photo130)) return false;
        if(!TextUtils.equals(another.photo604, photo604)) return false;
        if(!TextUtils.equals(another.photo807, photo807)) return false;
        if(!TextUtils.equals(another.photo1280, photo1280)) return false;
        if(!TextUtils.equals(another.photo2560, photo2560)) return false;

        return true;
    }

    public static class Builder {
        private final int id;
        private int albumId;
        private int ownerId;
        private String text;
        private int date;
        private String photo75;
        private String photo130;
        private String photo604;
        private String photo807;
        private String photo1280;
        private String photo2560;
        private int width;
        private int height;

        public Builder(int id) {
            this.id = id;
        }

        public Builder albumId(int albumId) {
            this.albumId = albumId;

            return this;
        }

        public Builder ownerId(int ownerId) {
            this.ownerId = ownerId;

            return this;
        }

        public Builder description(String text) {
            this.text = text;

            return this;
        }

        public Builder date(int timestamp) {
            this.date = timestamp;

            return this;
        }

        public Builder photo75(String photo75) {
            this.photo75 = photo75;

            return this;
        }

        public Builder photo130(String photo130) {
            this.photo130 = photo130;

            return this;
        }

        public Builder photo604(String photo604) {
            this.photo604 = photo604;

            return this;
        }

        public Builder photo807(String photo807) {
            this.photo807 = photo807;

            return this;
        }

        public Builder photo1280(String photo1280) {
            this.photo1280 = photo1280;

            return this;
        }

        public Builder photo2560(String photo2560) {
            this.photo2560 = photo2560;

            return this;
        }

        public Builder width(int width) {
            this.width = width;

            return this;
        }

        public Builder height(int height) {
            this.height = height;

            return this;
        }

        public Photo build() {
            return new Photo(this);
        }

    }

}
