package mekamello.galleryviewer.data.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


public class PhotoProvider extends ContentProvider {
    public static final String PROVIDER_NAME = "mekamello.galleryviewer.data.database.photos";
    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/photos");
    public static final int PHOTOS = 1;
    public static final int PHOTOS_ID = 2;
    public static final UriMatcher uriMatcher = getUriMatcher();
    public static UriMatcher getUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(PROVIDER_NAME, "photos", PHOTOS);
        matcher.addURI(PROVIDER_NAME, "photos/#", PHOTOS_ID);
        return matcher;
    }

    private PhotoDatabase database;

    @Override
    public boolean onCreate() {
        database = new PhotoDatabase(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String id = null;
        if(uriMatcher.match(uri) == PHOTOS_ID) {
            id = uri.getPathSegments().get(1);
        }

        return database.getPhotos(id, projection, selection, selectionArgs, sortOrder);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case PHOTOS:
                return "mekamello.galleryviewer.data.cache.dir/mekamello.galleryviewer.data.database.provider.photos";
            case PHOTOS_ID:
                return "mekamello.galleryviewer.data.cache.item/mekamello.galleryviewer.data.database.provider.photos";
        }

        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        try {
            return ContentUris.withAppendedId(
                    CONTENT_URI,
                    database.addNewPhoto(values)
            );

        } catch (Exception e) {

        }

        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        String id = null;
        if(uriMatcher.match(uri) == PHOTOS_ID) {
            id = uri.getPathSegments().get(1);
        }

        return database.deletePhoto(id);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String id = null;
        if(uriMatcher.match(uri) == PHOTOS_ID) {
            id = uri.getPathSegments().get(1);
        }

        return database.updatePhoto(id, values);
    }
}
