package mekamello.galleryviewer.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.text.TextUtils;


public class PhotoDatabase extends SQLiteOpenHelper {
    private static final int VERSION = 24;

    private static final String DATABASE_NAME = "database.db";
    private static final String TABLE_NAME = "photostorage";

    public static final String PHOTO_ORIGINAL_ID = "PHOTOORIGINALID";
    public static final String PHOTO_ALBUM_ID = "PHOTOALBUMID";
    public static final String PHOTO_OWNER_ID = "PHOTOOWNERID";
    public static final String PHOTO_TEXT = "PHOTOTEXT";
    public static final String PHOTO_DATE = "PHOTODATE";
    public static final String PHOTO_SIZE_75 = "PHOTOSIZE75";
    public static final String PHOTO_SIZE_130 = "PHOTOSIZE130";
    public static final String PHOTO_SIZE_604 = "PHOTOSIZE604";
    public static final String PHOTO_SIZE_807 = "PHOTOSIZE807";
    public static final String PHOTO_SIZE_1280 = "PHOTOSIZE1280";
    public static final String PHOTO_SIZE_2560 = "PHOTOSIZE2560";

    private static final String SQL_CREATE =
            "CREATE TABLE " + TABLE_NAME +
                    " (_id INTEGER PRIMARY KEY," +
                    PHOTO_ORIGINAL_ID + " INTEGER ," +
                    PHOTO_ALBUM_ID + " INTEGER ," +
                    PHOTO_OWNER_ID + " INTEGER ," +
                    PHOTO_TEXT + " TEXT ," +
                    PHOTO_DATE + " INTEGER ," +
                    PHOTO_SIZE_75 + " TEXT ," +
                    PHOTO_SIZE_130 + " TEXT ," +
                    PHOTO_SIZE_604 + " TEXT ," +
                    PHOTO_SIZE_807 + " TEXT ," +
                    PHOTO_SIZE_1280 + " TEXT ," +
                    PHOTO_SIZE_2560 + " TEXT )";

    private static final String SQL_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME ;

    PhotoDatabase(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP);
        onCreate(db);
    }

    Cursor getPhotos(String id, String[] projection, String selection, String[] selectionArgs, String sortOrder){
        SQLiteQueryBuilder sqliteQueryBuilder = new SQLiteQueryBuilder();
        sqliteQueryBuilder.setTables(TABLE_NAME);

        if(id != null) {
            sqliteQueryBuilder.appendWhere("_id" + " = " + id);
        }

        if(sortOrder == null || TextUtils.equals(sortOrder, "")) {
            sortOrder = PHOTO_ORIGINAL_ID;
        }

        Cursor cursor = sqliteQueryBuilder.query(getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);

        return cursor;
    }

    long addNewPhoto(ContentValues values) throws SQLException {
        long id = getWritableDatabase().insert(TABLE_NAME, "", values);
        if(id <=0 ) {
            throw new SQLException("Failed to add an image");
        }

        return id;
    }

    int deletePhoto(String id) {
        if(id == null) {
            return getWritableDatabase().delete(TABLE_NAME, null , null);
        } else {
            return getWritableDatabase().delete(TABLE_NAME, "_id=?", new String[]{id});
        }
    }

    int updatePhoto(String id, ContentValues values) {
        if(id == null) {
            return getWritableDatabase().update(TABLE_NAME, values, null, null);
        } else {
            return getWritableDatabase().update(TABLE_NAME, values, "_id=?", new String[]{id});
        }
    }
}
