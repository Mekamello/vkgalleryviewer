package mekamello.galleryviewer.data.repositories;

import android.content.Context;
import android.support.annotation.NonNull;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKSdk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mekamello.galleryviewer.GalleryApplication;
import mekamello.galleryviewer.buisness.GalleryInteractor;
import mekamello.galleryviewer.data.network.models.PhotoModel;
import mekamello.galleryviewer.data.network.models.PhotoResponse;
import mekamello.galleryviewer.data.network.PhotoVkService;
import mekamello.galleryviewer.data.database.models.Photo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GalleryRepositoryImpl implements GalleryRepository {
    private final PhotoVkService network;
    private final PhotoProviderRepository database;

    private int maxCount = 0;

    public GalleryRepositoryImpl(@NonNull Context context, @NonNull PhotoVkService network) {
        this.network = network;
        this.database = new PhotoProviderRepositoryImpl(context);
    }

    @Override
    public void getPhotos(int offset, int count, @NonNull GalleryInteractor.Callback callback) {
        VKAccessToken token = VKAccessToken.currentToken();

        String from = Integer.toString(offset);
        String size = Integer.toString(count);

        if(offset > maxCount) {
            return;
        }

        if(token != null) {
            network
                    .getAllPhotos(token.accessToken, from, size, VKSdk.getApiVersion())
                    .enqueue(new Callback<PhotoResponse>() {
                        @Override
                        public void onResponse(Call<PhotoResponse> call, Response<PhotoResponse> response) {
                            if(response.isSuccessful()) {
                                PhotoResponse.Response photoResponse = response.body().get;

                                if(photoResponse != null) {
                                    List<PhotoModel> payload = photoResponse.payload;

                                    maxCount = response.body().get.count;

                                    if(payload != null) {
                                        List<Photo> convertedPhotoList = convertToPhotoList(payload);
                                        database.storePhotoList(convertedPhotoList);

                                        callback.onSuccess(convertedPhotoList, maxCount);
                                    }  else {
                                        callback.onFailure(new Throwable("payloads is null"));
                                    }
                                }


                            } else {
                                callback.onFailure(new Throwable("response not successful"));
                            }
                        }

                        @Override
                        public void onFailure(Call<PhotoResponse> call, Throwable t) {
                            List<Photo> stored = new ArrayList<>(database.getStoredPhotoList());

                            if(stored.isEmpty()) {
                                callback.onFailure(t);
                            } else {
                                maxCount = stored.size();
                                callback.onSuccess(getPhotoListWithOffset(offset, maxCount, stored), maxCount);
                            }
                        }
                    });

        } else {
            callback.onFailure(new Throwable("Login first"));
        }
    }

    private List<Photo> getPhotoListWithOffset(int offset, int maxCount, List<Photo> allData) {
        List<Photo> result = new ArrayList<>();

        int size = allData.size();

        for(int i = 0; i < maxCount; i++) {
            int index = i + offset;
            if(index < size) {
                result.add(allData.get(index));
            } else {
                return result;
            }
        }

        return result;
    }

    private List<Photo> convertToPhotoList(List<PhotoModel> response){
        List<Photo> result = new ArrayList<>();
        for(PhotoModel model : response) {
            result.add(convertToPhoto(model));
        }

        return result;
    }

    private Photo convertToPhoto(PhotoModel model) {
        return new Photo.Builder(model.id)
                .albumId(model.albumId)
                .ownerId(model.ownerId)
                .date(model.timestamp)
                .description(model.text)
                .photo75(model.photo75)
                .photo130(model.photo130)
                .photo604(model.photo604)
                .photo807(model.photo807)
                .photo1280(model.photo1280)
                .photo2560(model.photo2560)
                .width(model.width)
                .height(model.height)
                .build();
    }

}
