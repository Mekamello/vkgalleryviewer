package mekamello.galleryviewer.data.repositories;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import mekamello.galleryviewer.data.database.models.Photo;

public interface PhotoMemoryRepository {
    Bitmap getBitmap(@NonNull Photo photo, @NonNull Photo.Size size);
    Bitmap getBitmap(@NonNull Photo photo, @NonNull Photo.Size size, int width, int height);
}
