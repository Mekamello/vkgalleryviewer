package mekamello.galleryviewer.data.repositories;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import mekamello.galleryviewer.data.database.models.Photo;

public interface PhotoProviderRepository {
    void storePhotoList(@NonNull List<Photo> data);

    List<Photo> getStoredPhotoList();

    void refresh(@Nullable Callback callback);

    interface Callback {
        void onRefreshed(@NonNull Cursor cursor);
    }
}
