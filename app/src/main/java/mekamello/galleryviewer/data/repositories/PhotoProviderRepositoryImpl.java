package mekamello.galleryviewer.data.repositories;


import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.CursorLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mekamello.galleryviewer.data.database.PhotoDatabase;
import mekamello.galleryviewer.data.database.models.Photo;

import static mekamello.galleryviewer.data.database.PhotoProvider.CONTENT_URI;

public class PhotoProviderRepositoryImpl implements PhotoProviderRepository{
    private final Context context;

    private Callback callback;

    public PhotoProviderRepositoryImpl(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public void storePhotoList(@NonNull List<Photo> data) {
        List<Photo> dataToStore = new ArrayList<>(data);
        Cursor cursor = getCursor();

        if(cursor != null) {
            int count = cursor.getCount();

            while (cursor.moveToNext()){
                 Photo stored = new Photo(cursor);

                if(dataToStore.contains(stored)) {

                    int containsIndex = dataToStore.indexOf(stored);

                    if(containsIndex == -1){
                        continue;
                    }

                     if (stored.isContainEqualData(dataToStore.get(containsIndex))) {
                         dataToStore.remove(stored);
                     } else {
                         context.getContentResolver()
                                 .update(
                                         CONTENT_URI,
                                         generateContentValuesFor(dataToStore.get(containsIndex)),
                                         PhotoDatabase.PHOTO_ORIGINAL_ID,
                                         new String[] { Integer.toString(stored.id) });

                         dataToStore.remove(stored);
                     }
                 } else {
//                     context.getContentResolver()
//                             .delete(
//                                     CONTENT_URI,
//                                     PhotoDatabase.PHOTO_ORIGINAL_ID,
//                                     new String[] { Integer.toString(stored.id) });
                 }
             }

            cursor.close();
        }

        load(dataToStore);
    }

    @Override
    public List<Photo> getStoredPhotoList() {
        List<Photo> storedPhoto = new ArrayList<>();
        Cursor cursor = getCursor();

        if(cursor != null) {
            while (cursor.moveToNext()){
                Photo stored = new Photo(cursor);
                storedPhoto.add(stored);
            }
        }

        Collections.sort(
                storedPhoto,
                (o1, o2) -> o1.date < o2.date ? 0 : 1);

        return storedPhoto;
    }


    @Override
    public void refresh(@Nullable Callback callback) {
        this.callback = callback;

        Cursor cursor = getCursor();

        if(callback != null) {
            callback.onRefreshed(cursor);
        }
    }

    public Cursor getCursor() {
        CursorLoader cursorLoader = new CursorLoader(context, CONTENT_URI, null, null, null, null);
        return cursorLoader.loadInBackground();
    }

    private ContentValues generateContentValuesFor(Photo photo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PhotoDatabase.PHOTO_ORIGINAL_ID, photo.id);
        contentValues.put(PhotoDatabase.PHOTO_ALBUM_ID, Math.abs(photo.albumId));
        contentValues.put(PhotoDatabase.PHOTO_OWNER_ID, photo.ownerId);
        contentValues.put(PhotoDatabase.PHOTO_TEXT, photo.text);
        contentValues.put(PhotoDatabase.PHOTO_DATE, photo.date);
        contentValues.put(PhotoDatabase.PHOTO_SIZE_75, photo.photo75);
        contentValues.put(PhotoDatabase.PHOTO_SIZE_130, photo.photo130);
        contentValues.put(PhotoDatabase.PHOTO_SIZE_604, photo.photo604);
        contentValues.put(PhotoDatabase.PHOTO_SIZE_807, photo.photo807);
        contentValues.put(PhotoDatabase.PHOTO_SIZE_1280, photo.photo1280);
        contentValues.put(PhotoDatabase.PHOTO_SIZE_2560, photo.photo2560);
        return contentValues;
    }


    private void load(@NonNull List<Photo> data) {
        for(Photo each: data) {
            context.getContentResolver().insert(CONTENT_URI, generateContentValuesFor(each));
        }

        if(callback != null) {
            refresh(callback);
        }
    }

}
