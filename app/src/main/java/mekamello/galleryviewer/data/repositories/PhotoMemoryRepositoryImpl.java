package mekamello.galleryviewer.data.repositories;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.util.LruCache;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import mekamello.galleryviewer.data.database.models.Photo;
import okhttp3.internal.DiskLruCache;

public class PhotoMemoryRepositoryImpl implements PhotoMemoryRepository {
    private static PhotoMemoryRepository instance;

    public static PhotoMemoryRepository getInstance(Context context) {
        if (instance == null) {
            instance = new PhotoMemoryRepositoryImpl(context);
        }

        return instance;
    }

    @NonNull
    private LruCache<String, Bitmap> cache;

    private final Context context;

    private PhotoMemoryRepositoryImpl(Context context) {
        this.context = context;

        int memClass = ((ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
        int cacheSize = 1024 * 1024 * memClass / 8;

        cache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getByteCount();
            }
        };
    }


    @Override
    public Bitmap getBitmap(@NonNull Photo photo, @NonNull Photo.Size size) {
        String key = getKey(photo, size);

        Bitmap image = null;

        if (key == null) {
            key = searchForKey(photo, size);
        }

        if (key == null) {
            String message = "Can't find url for photo";

            Log.e("PhotoMemoryCacheHelper", "Photo without url", new Throwable(message));

        } else {
                try {
                    URL url = new URL(key);

                    image = BitmapFactory.decodeStream(url.openStream());

                } catch (Exception e) {
                    return getScaledDownBitmap(photo);
                }
        }

        if(image == null) {
            image = getScaledDownBitmap(photo);
        }

        return image;
    }

    @Override
    public Bitmap getBitmap(@NonNull Photo photo, @NonNull Photo.Size size, int width, int height) {
        Bitmap bitmap = null;

        String key = getKey(photo, size);

        if (key == null) {
            key = searchForKey(photo, size);
        }

        if (key == null) {
            String message = "Can't find url for photo";

            Log.e("PhotoMemoryCacheHelper", "Photo without url", new Throwable(message));

        } else {
            bitmap = cache.get(key);

            if(bitmap == null) {
                try {
                    URL url = new URL(key);

                    bitmap = decodeSampledBitmapFromResource(url, width, height);

                    if (bitmap != null) {
                        saveBitmap(key, bitmap);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    cache.evictAll();
                }
            }
        }

        if(bitmap == null) {
            bitmap = getScaledDownBitmap(photo);
        }

        return bitmap;
    }

    public void saveBitmap(String key, Bitmap image) {
        if (cache.get(key) == null) {
            cache.put(key, image);
        }
    }

    private static String getKey(@NonNull Photo photo, @NonNull Photo.Size size) {
        switch (size) {
            case Photo75:
                return photo.photo75;
            case Photo130:
                return photo.photo130;
            case Photo604:
                return photo.photo604;
            case Photo807:
                return photo.photo807;
            case Photo1280:
                return photo.photo1280;
            case Photo2560:
                return photo.photo2560;

            default:
                return photo.photo75;
        }
    }

    private String searchForKey(@NonNull Photo photo, @NonNull Photo.Size size) {
        String key = getKey(photo, size);

        Photo.Size[] allPhotoSizes = Photo.Size.values();

        if (key == null) {
            for (int i = allPhotoSizes.length - 1; i > 0; i--) {
                String anotherKey = getKey(photo, allPhotoSizes[i]);
                if (anotherKey != null) {
                    return anotherKey;
                }
            }
        }

        return key;

    }

    private Bitmap getScaledDownBitmap(@NonNull Photo photo) {
        Bitmap bitmap = null;

        Photo.Size[] allPhotoSizes = Photo.Size.values();

        for (int i = allPhotoSizes.length - 1; i > 0; i--) {
            String anotherKey = getKey(photo, allPhotoSizes[i]);

            if(bitmap == null) {
                bitmap = cache.get(anotherKey);
            }
        }

        return bitmap;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(URL url, int reqWidth, int reqHeight) throws IOException {
        Bitmap bitmap;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeStream(url.openStream(), null, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        bitmap = BitmapFactory.decodeStream(url.openStream(), null, options);

        return bitmap;
    }
}
