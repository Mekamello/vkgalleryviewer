package mekamello.galleryviewer.data.repositories;

import android.support.annotation.NonNull;

import mekamello.galleryviewer.buisness.GalleryInteractor;


public interface GalleryRepository {
    void getPhotos(int offset, int count, @NonNull GalleryInteractor.Callback callback);
}
