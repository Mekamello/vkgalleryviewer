package mekamello.galleryviewer.base;

import android.os.Handler;
import android.os.Looper;

public class MainThreadImpl implements MainThread {
    private static MainThread instance;
    public static MainThread getInstance() {
        if(instance == null) {
            instance = new MainThreadImpl();
        }

        return instance;
    }

    private Handler handler;

    public MainThreadImpl() {
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void post(Runnable runnable) {
        handler.post(runnable);
    }
}
