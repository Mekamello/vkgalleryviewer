package mekamello.galleryviewer.base;

import android.support.annotation.NonNull;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadExecutorImpl implements Executor {
    private static volatile ThreadExecutorImpl instance;

    public static Executor getInstance() {
        if(instance ==  null) {
            instance = new ThreadExecutorImpl();
        }

        return instance;
    }

    private static final int POOL_SIZE = 3;
    private static final int POOL_SIZE_MAX = 6;
    private static final int LIVE_TIME = 100;
    private static final BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();

    private ThreadPoolExecutor poolExecutor;

    public ThreadExecutorImpl() {
        poolExecutor = new ThreadPoolExecutor(
                POOL_SIZE,
                POOL_SIZE_MAX,
                LIVE_TIME,
                TimeUnit.SECONDS,
                queue);
    }

    @Override
    public void execute(@NonNull BaseInteractor interactor) {
        poolExecutor.submit(() -> {
            interactor.run();
            interactor.onFinished();
        });
    }
}
