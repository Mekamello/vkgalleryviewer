package mekamello.galleryviewer.base;

import android.support.annotation.NonNull;

public interface Executor {
    void execute(@NonNull BaseInteractor interactor);
}
