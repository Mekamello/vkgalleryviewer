package mekamello.galleryviewer.base;

public interface Interactor {
    void execute();
}
