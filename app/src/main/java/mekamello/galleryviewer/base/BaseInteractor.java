package mekamello.galleryviewer.base;

import android.support.annotation.NonNull;

public abstract class BaseInteractor implements Interactor {
    @NonNull
    protected final Executor executor;
    @NonNull
    protected final MainThread mainThread;

    protected volatile boolean isCanceled;
    protected volatile boolean isRunning;

    public BaseInteractor(@NonNull Executor executor, @NonNull MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
    }

    public abstract void run();

    public void cancel() {
        isCanceled = true;
        isRunning = false;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void onFinished() {
        isRunning = false;
        isCanceled = false;
    }

    @Override
    public void execute() {
        isRunning = true;

        executor.execute(this);
    }

}
