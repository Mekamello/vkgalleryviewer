package mekamello.galleryviewer.base;

public interface MainThread {
    void post(final Runnable runnable);
}
