package mekamello.galleryviewer.dagger.application;

import javax.inject.Singleton;

import dagger.Component;
import mekamello.galleryviewer.dagger.gallery.GalleryDisplayComponent;
import mekamello.galleryviewer.dagger.gallery.GalleryModule;
import mekamello.galleryviewer.dagger.gallery.GalleryPreviewComponent;
import mekamello.galleryviewer.dagger.gallery.GalleryPreviewModule;

@Component(modules = {AppModule.class, NetworkModule.class, DatabaseModule.class})
@Singleton
public interface AppComponent {
    GalleryDisplayComponent plus(GalleryModule galleryModule);
    GalleryPreviewComponent plus(GalleryPreviewModule previewModule);
}
