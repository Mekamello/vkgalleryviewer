package mekamello.galleryviewer.dagger.application;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import mekamello.galleryviewer.dagger.gallery.GalleryScope;
import mekamello.galleryviewer.data.network.PhotoVkService;
import mekamello.galleryviewer.data.repositories.GalleryRepository;
import mekamello.galleryviewer.data.repositories.GalleryRepositoryImpl;
import mekamello.galleryviewer.data.repositories.PhotoMemoryRepository;
import mekamello.galleryviewer.data.repositories.PhotoMemoryRepositoryImpl;
import mekamello.galleryviewer.data.repositories.PhotoProviderRepositoryImpl;


@Module
public class DatabaseModule {
    @Provides
    @Singleton
    PhotoMemoryRepository providePhotoMemoryRepository(Context context) {
        return PhotoMemoryRepositoryImpl.getInstance(context);
    }

    @Provides
    @Singleton
    PhotoProviderRepositoryImpl providePhotoProviderRepository(Context context) {
        return new PhotoProviderRepositoryImpl(context);
    }

    @Provides
    @Singleton
    GalleryRepository provideGalleryRepository(Context context, PhotoVkService service) {
        return new GalleryRepositoryImpl(context, service);
    }}
