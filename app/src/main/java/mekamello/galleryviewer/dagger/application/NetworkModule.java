package mekamello.galleryviewer.dagger.application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import mekamello.galleryviewer.data.network.PhotoVkService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class NetworkModule {
    public static final String VK_API_URL= "https://api.vk.com/method/";

    @Provides
    @Singleton
    PhotoVkService providePhotoVkService(Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(VK_API_URL)
                .build();

        return retrofit.create(PhotoVkService.class);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }
}
