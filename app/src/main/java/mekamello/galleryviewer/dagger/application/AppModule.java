package mekamello.galleryviewer.dagger.application;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import mekamello.galleryviewer.base.Executor;
import mekamello.galleryviewer.base.MainThread;
import mekamello.galleryviewer.base.MainThreadImpl;
import mekamello.galleryviewer.base.ThreadExecutorImpl;


@Module
public class AppModule {
    private final Context applicationContext;

    public AppModule(@NonNull Context context) {
        applicationContext = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return applicationContext;
    }

    @Provides
    @Singleton
    Executor executor() {
        return ThreadExecutorImpl.getInstance();
    }

    @Provides
    @Singleton
    MainThread mainThread() {
        return MainThreadImpl.getInstance();
    }
}
