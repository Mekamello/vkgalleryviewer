package mekamello.galleryviewer.dagger.gallery;

import dagger.Subcomponent;
import mekamello.galleryviewer.ui.view.gallery.display.GalleryActivity;
import mekamello.galleryviewer.ui.view.gallery.display.GalleryFragment;

@Subcomponent(modules = {GalleryModule.class})
@GalleryScope
public interface GalleryDisplayComponent {
    void inject(GalleryActivity activity);
    void inject(GalleryFragment fragment);
}
