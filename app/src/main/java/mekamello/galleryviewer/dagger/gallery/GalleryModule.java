package mekamello.galleryviewer.dagger.gallery;

import dagger.Module;
import dagger.Provides;
import mekamello.galleryviewer.base.Executor;
import mekamello.galleryviewer.base.MainThread;
import mekamello.galleryviewer.data.repositories.GalleryRepository;
import mekamello.galleryviewer.ui.presenter.gallery.display.GalleryPresenter;
import mekamello.galleryviewer.ui.presenter.gallery.display.GalleryPresenterImpl;

@Module
public class GalleryModule {

    @Provides
    @GalleryScope
    GalleryPresenter provideGalleryPresenter(Executor executor, MainThread mainThread, GalleryRepository repository) {
        return new GalleryPresenterImpl(executor, mainThread, repository);
    }


//    @Provides
//    @GalleryScope
//    GalleryAdapterPresenter provideGalleryAdapterPresenter(Executor executor, MainThread mainThread, GalleryRepository repository) {
//        return new GalleryAdapterPresenterImpl(executor, mainThread, repository);
//    }

}
