package mekamello.galleryviewer.dagger.gallery;

import dagger.Subcomponent;
import mekamello.galleryviewer.ui.view.photo.display.GalleryPreviewActivity;

@Subcomponent(modules = {GalleryPreviewModule.class})
@GalleryScope
public interface GalleryPreviewComponent {
    void inject(GalleryPreviewActivity activity);

}
