package mekamello.galleryviewer.dagger.gallery;

import dagger.Module;
import dagger.Provides;
import mekamello.galleryviewer.base.Executor;
import mekamello.galleryviewer.base.MainThread;
import mekamello.galleryviewer.data.repositories.PhotoProviderRepositoryImpl;
import mekamello.galleryviewer.ui.presenter.photo.adapter.GalleryPreviewAdapterPresenter;
import mekamello.galleryviewer.ui.presenter.photo.adapter.GalleryPreviewAdapterPresenterImpl;
import mekamello.galleryviewer.ui.presenter.photo.display.GalleryPreviewPresenter;
import mekamello.galleryviewer.ui.presenter.photo.display.GalleryPreviewPresenterImpl;

@Module
public class GalleryPreviewModule {

    @Provides
    @GalleryScope
    GalleryPreviewAdapterPresenter provideGalleryAdapterPresenter(Executor executor, MainThread mainThread, PhotoProviderRepositoryImpl repository) {
        return new GalleryPreviewAdapterPresenterImpl(executor, mainThread, repository);
    }

    @Provides
    @GalleryScope
    GalleryPreviewPresenter provideGalleryPresenter(GalleryPreviewAdapterPresenter adapterPresenter) {
        return new GalleryPreviewPresenterImpl(adapterPresenter);
    }

}
