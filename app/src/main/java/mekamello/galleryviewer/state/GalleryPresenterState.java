package mekamello.galleryviewer.state;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class GalleryPresenterState implements Parcelable{
    private static final String KEY = "mekamello.galleryviewer.state.GalleryPresenterState";

    public final boolean isAuthorizationStarted;

    public GalleryPresenterState() {
        isAuthorizationStarted = false;
    }

    public GalleryPresenterState(boolean isAuthorizationStarted) {
        this.isAuthorizationStarted = isAuthorizationStarted;
    }

    private GalleryPresenterState(@NonNull Bundle bundle) {
        this((GalleryPresenterState)bundle.getParcelable(KEY));
    }

    private GalleryPresenterState(@Nullable GalleryPresenterState state) {
        isAuthorizationStarted = state != null && state.isAuthorizationStarted;
    }

    @NonNull
    public static GalleryPresenterState getFrom(@Nullable Bundle bundle) {
        return bundle == null ?
                new GalleryPresenterState() :
                new GalleryPresenterState(bundle);
    }

    public static void saveTo(@NonNull Bundle bundle, @NonNull GalleryPresenterState newState) {
        bundle.putParcelable(KEY, newState);
    }

    //region PARCELABLE
    protected GalleryPresenterState(Parcel in) {
        isAuthorizationStarted = in.readByte() != 0;
    }

    public static final Creator<GalleryPresenterState> CREATOR = new Creator<GalleryPresenterState>() {
        @Override
        public GalleryPresenterState createFromParcel(Parcel in) {
            return new GalleryPresenterState(in);
        }

        @Override
        public GalleryPresenterState[] newArray(int size) {
            return new GalleryPresenterState[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isAuthorizationStarted ? 1 : 0));
    }
    //endregion PARCELABLE

}
