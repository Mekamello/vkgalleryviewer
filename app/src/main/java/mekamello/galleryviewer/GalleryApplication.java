package mekamello.galleryviewer;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;

import java.io.IOException;

import mekamello.galleryviewer.dagger.application.AppComponent;
import mekamello.galleryviewer.dagger.application.AppModule;
import mekamello.galleryviewer.dagger.application.DaggerAppComponent;
import mekamello.galleryviewer.ui.view.gallery.display.GalleryActivity;


public class GalleryApplication extends Application {
    public static final String INVALID_TOKEN_EXTRA = "mekamello.galleryviewer.INVALID_TOKEN";

    private AppComponent component;

    private VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            Log.d("GalleryApplication", "token expired " + Boolean.toString(newToken != null));

            if (newToken == null) {
                sendInvalidTokenBroadcast(true);
            }
        }
    };

    @NonNull
    public static GalleryApplication get(@NonNull Context context) {
        return (GalleryApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        vkAccessTokenTracker.startTracking();

        VKSdk.initialize(getApplicationContext());

    }

    @NonNull
    public AppComponent applicationComponent() {
        return component;
    }

    public void sendInvalidTokenBroadcast(boolean invalidToken) {
        Intent intent = new Intent(GalleryActivity.BROADCAST_INVALID_TOKEN);
        intent.putExtra(INVALID_TOKEN_EXTRA, invalidToken);
        sendBroadcast(intent);
    }
}
